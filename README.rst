This project is no longer maintained.

The contents of this repository are still available in the Git
source code management system.  To see the contents of this
repository before it reached its end of life, please check out the
previous commit with "git checkout HEAD^1".

The content of tap-as-a-service-tempest-plugin was moved to
neutron-tempest-plugin and maintained there:
https://opendev.org/openstack/neutron-tempest-plugin/src/branch/master/neutron_tempest_plugin/tap_as_a_service

For any further questions, please email
openstack-discuss@lists.openstack.org or join #openstack-dev on
OFTC.

